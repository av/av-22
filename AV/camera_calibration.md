# Intrinsic Camera Calibration

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)
## [Index](../Readme.md)
## Contents
* [Introduction](#introduction)
* [Checkerboard Image Data](#checkerboard-image-data)
* [Uncompress Images](#uncompress-the-image-topic)
* [Intrinsic Calibration](#do-intrinsic-calibration)
* [Fix the `/tmp` Folder Issue](#fix-the-tmp-folder-issue)

# Introduction

**Intrinsic calibration** provides a model by which each pixel specifies the direction of a ray from the camera's optical center into the world.  So, for instance, if you know the depth of an object along a pixel ray, you can infer exactly the 3D location of the object relative to the camera.  This is in contrast to **extrinsic calibration**, which specifies the pose (i.e. location and orientation) of a sensor.  

**Before you begin**: The Docker image has been updated on *October 5, 2022* to include the necessary calibration libraries described here.  You will need to update your HPCC environment as explained in [HPCC_ROS](../Setup/HPCC_ROS.md).  This includes the following: update the `.bashrc` file (then source it), update the `.rosinit` file, and then do `docker_pull` to get the new Docker image called `foxy_noetic.sif`.

# Checkerboard Image Data

Intrinsic camera calibration is typically performed using a checkerboard pattern that is moved infront of the camera.  Here we will calibrate the instructor's Turtlebot's camera using a pre-recorded rosbag containing images of a checkerboard.  Let's start by viewing the video from the rosbag.  First play the rosbag in a loop with:
```
R:~$ ros2 bag play ~/av/data/msu_bags_v2/intrinsic_cali --loop
```
This will display a couple warnings about topics it is unable to interpret, but that's okay as you'll see if you look at the topic warnings, we don't need those topics.  Now have a look at the clip using `rqt`.  You should see the checkboard.  Count the number of internal corners, both vertically and horizontally.  We'll need to specify this in the calibration call.

# Uncompress the Image Topic

Also, use `rqt` to determine the topic name and type of the published image.  You should see that the name is: `/raspicam_node/image/compressed` and the message type is: `sensor_msg/msg/CompressedImage`.  That is telling us that the images in the rosbag are jpeg compressed.  Now the camera calibration utility requires uncompressed images, so we will need to uncompress them.

ROS has image compression and decompression tools that can run in real time on a published topic like we have.  To decompress that image topic, use the command:
```bash
R:~$ ros2 run image_transport republish compressed raw --ros-args --remap /in/compressed:=/raspicam_node/image/compressed --remap /out:=/image
```
This decompresses the image from topic `/raspicam_node/image/compressed` and republishes it to topic `/image`.  The calibration code below assumes raw images are published to the `/image` topic.  So keep this running and we should be ready to run camera calibration.  It is a good idea to confirm that you can view the raw image output in topic `/image` using `rqt` like you did above.  You may need to press the refresh button to see the new topics being published.

# Do Intrinsic Calibration

If you can see the checkerboard in being published on the `/image` topic, you are ready to do intrinsic calibration.  The following is the ROS calibration package you can use for this:
```bash
R:~$ ros2 run camera_calibration cameracalibrator --size 6x8 --square 0.026 -c raspicam --no-service-check
```
Here the arguments are:

**`--size VxH`** where V is the number of internal corners vertically and H is the number of corners horizontally.

**`--square 0.026`** the side of a checkerboard square is 0.026 meters.  For intrinsic parameters this does not matter.

**`--no-service-check`** is just because we are playing a rosbag, and will not be able to call a ROS service to set the `camera_info` on the Turtlebot.  By default the calibration will query the camera for `camera_info`.

You should see an output window with the checkerboard and the detected corners highlighted in colored dots.  When the `Calibration` circle is highlighted, you can click on it.  When that is done, which may take a while, you can save the parameters by clicking on the `Save` circle. You'll notice it saves the data to a hard-coded location: `/tmp/calibrationdata.tar.gz`.  To extract the data from this use:
```
R:~$ mkdir -p ~/mytemp
R:~$ cd ~/mytemp
R:~/mytemp$ tar -xzf /tmp/calibrationdata.tar.gz
```
Finally, on the HPCC cluster `/tmp` folder is shared between users so if one user leaves their `calibrationdata.tar.gz` file in the `/tmp` folder, no one else will be able to save theirs.  Thus, right after you extract the contents of the file, delete it with:
```
R:~$ rm /tmp/calibrationdata.tar.gz
```
Now have a look in the file `ost.txt` or `ost.yaml` for the output parameters.

# Fix the `tmp` Folder Issue

The file name that the calibration data are saved to is hard-coded.  That's not great.  On the other hand, ROS is fully open source, so you are free to go in and change the code.  It is actually not hard to make a small change to the code like this.  The following is how you can change the output folder or the file name where the calibration data will be saved.  This section is purely optional, and if you prefer you can continue to use the default `camera_calibration` package.

The first thing is to identify the `Github` repo in which the ROS package is located.  In this case the `camera_calibration` package is located in the [image_pipeline](https://github.com/ros-perception/image_pipeline) repository.  Clone this repo into your workspace `src` folder and switch to the `foxy` branch with the following commands:
```
R:~$ cd av/ros_ws/src/
R:~/av/ros_ws/src$ git clone https://github.com/ros-perception/image_pipeline.git
R:~/av/ros_ws/src$ cd image_pipeline
R:~/av/ros_ws/src/image_pipeline$ git checkout foxy
```
If you wish, you can use a different workspace for this task to keep this code separate from your other packages.  Also, if you fail to switch to the correct branch, you'll get various errors when you try building and/or running the code.  

In VSCode, open the `camera_calibration` folder.  Then use the `Find in Files` option to search for the `/tmp/calibrationdata.tar.gz` string.  When you find the location in the code where this occurs, you can simply change it.  For example, you might replace it with `calibrationdata.tar.gz`.  If you do this, then the code will write the file to the folder from which the package is run instead of in the `/tmp` folder.  

After you have made your change to the code, then build it as follows:
```
R:~/av/ros_ws/src/image_pipeline$ cd ~/av/ros_ws
R:~/av/ros_ws$ colcon build --symlink-install --packages-select camera_calibration
```
Finally, source the overlay
```
R:~/av/ros_ws$ source install/setup.bash
```
And you are ready to run calibration again with the same command as above.  With this change you now have the `camera_calibration` package in both your base ROS install folder and in your workspace folder.  In ROS when you source a workspace `install/setup.bash` file, the workspace packages will take precedence over the base packages.  Thus, when you run `camera_calibration` with the above arguments, your new workspace version will run.



