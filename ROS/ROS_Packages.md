# Start Programming with ROS 2

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)
### [Index](../Readme.md)
## Content

1. [ROS 2 Prerequisites](#1-ros-2-prerequisites)
   * [Underlay](#underlay)
   * [Overlay](#overlay)
   * [Package Precedence](#package-precedence)
   * [No More Master: Change from ROS 1](#no-more-master-change-from-ros-1)
   * [Python Environment](#python-environment)
   * [Create A Workspace](#create-a-workspace)
   * [Filename Requirements](#filename-requirements)
   * [Workspace Organization](#workspace-organization)
2. [Create ROS Packages](#2-create-ros-packages)
   * [A Bare Minimum Package](#a-bare-minimum-package)
   * [A Package with Publisher and Subscriber nodes](#a-package-with-publisher-and-subscriber-nodes)

___

Here are preliminaries for getting started with ROS 2 after you have installed ROS 2 and configured your shell as described in [Setup Your Environment](../README.md#1-setup-your-environment).  


# 1. ROS 2 Prerequisites

This section describes the setup steps needed before you run ROS 2 commands.  It also discusses some key changes from ROS 1.  

## Underlay

The **underlay** is a script that initializes the ROS distro that you wish to run.  In this class the Remote Desktop has ROS Humble installed in `C:\dev\ros2_humble`.  Running, (or sourcing when in linux), either the `setup.ps1` or `local_setup.ps1` script will initialize your ROS environment.  In PowerShell you can do this simply with 
````
C:\dev\ros2_humble\local_setup.ps1
````
Or, if you have configured your shell as described in [Setup/Shell_Config.md](../Setup/Shell_Config.md), you can simply type:
```
rosunder
```
You can now use ROS commands available in this distro.

## Overlay
Your ROS development will occur in a workspace where you will create and compile packages and nodes within packages.  In order to run nodes and packages in *your* workspace you need to first tell ROS where to find your workspace.  You do this by running the overlay.  The workspace will have a `setup.ps1` script located in the `install` folder which is your overlay script.  If your workspace is located at `M:\av\ros_ws`, then you can source your overlay with:
```
M:\av\ros_ws\install\setup.ps1
```
Now running the `setup.ps1` script will internally run the underlay script.  So, if you have a workspace that has already been built, and you want to run a package in it, then you do not need to run the underlay first; you can simply run the workspace setup script which will take care of sourcing the underlay.

### Troubleshooting: If your just-built package won't run ...

If you are attempting to run one of your own packages, and ROS is unable to find it, there is a good chance that you forgot to run the overlay in that project's workspace. 

## Package Precedence

Packages in the overlay take precedence to the underlay.  Since ROS is free and open source, you have access to all the packages internal to ROS.  If you wish to modify one of those, you can add its source code to your workspace, and modify it, and then build it.  Then, if you have sourced your overlay, any ROS command that uses that package will use your version of it, as the overlay version takes precedence over the internal version.  This makes it easy to test changes to ROS and integrate in new functionality.  It also means you should be careful not to unintentionally create a package with the same name as an internal ROS package.

## No More Master: Change From ROS 1

In ROS 1, the master process acts as a master-of-ceremonies for ROS nodes.  Whenever a node starts,it communicates with that master, who provides it with ports to communicate with other nodes. The ROS master is required to be running all the time nodes are communicating.  Technical details are available here: [http://wiki.ros.org/ROS/Technical%20Overview](http://wiki.ros.org/ROS/Technical%20Overview).  In ROS 1, the master process is typically started with the `roscore` command, or else with a `roslaunch` command.  

Now a big change introduced by ROS 2 is that there is *no longer a master process*.   ROS 2 uses a [Data Distribution Service (DDS)](https://docs.ros.org/en/humble/Installation/DDS-Implementations.html#) middleware to handle communication, as described by this [article](https://design.ros2.org/articles/ros_on_dds.html).  The default DDS is eProsima's Fast DDS.  Using this default DDS, ROS nodes running on the same computer or within the same subnet can discover each other without requring a separate master process.  This change from ROS 1 simplifies the task of starting up ROS nodes, and makes ROS 2 less susceptible to a single point of failure which could occur in the master process.

## Python Environment

ROS comes with Python and a number of packages pre-installed.  However, you may need to use additional packages in your ROS nodes.  The best way to do that is with a Python virtual environment on top of the ROS Python environment.  How to set this up is explained in [Setup/Python_Environment](../Setup/Python_Environment.md).  

## Create a Workspace

To create a workspace is simple: create a folder with a subfolder called `src`.  On the Remote Desktop we created a workspace called `ros_ws` like this:
```
md M:\av\ros_ws\src
```
All the packages with their source code will be put into `src`.  When at least one package is built, then the workspace will be populated with three additional folders: `log`, `install`, and `build`.  

## Filename Requirements

There are a few things to be aware in naming packages and nodes.  
* Do **not** use capital letters in a package name or node name.  For example, this is **bad**: `myNode`, but this is **good**: `my_node`.  
* Windows has a default limitation of 260 characters for filenames including the full path.  While this seems more than long enough, the build process creates intermediate files with names incorporating the path to the workspace.  This means that if the path plus name of your workspace is long, this can result in filename overflow during build.  Thus, in Windows, it is advisable to keep the workspace path short, like we did with `M:\av\ros_ws`. Another way to reduce the chance of filename overflow is using the `--merge-install` option during building.  Finally, if your build crashes, you can usually find the reason by examining the log file generated during the build.

## Workspace Organization

The following figure shows the workspace folder structure we are using in this course. Folders are shown in blue boxes and key files with red text.  (Some folders and files are omitted.):

![Workspace](.Images/ROS_Workspace.png)

The workspace here is called `ros_ws` (although you can use any name you like) and contains 4 folders: 
* `src`: All the package source code.
* `log`:  Logs of the `colcon` build process. Look here when there are difficult-to-understand failures during building.
* `build`: Temporary build files generated by `colcon`.
* `install`: Code generated by `colcon`.  Includes the overlay scripts: `setup.ps1` for PowerShell, and `setup.bash` for Ubuntu.

The build command (using `colcon`) should always be called at the top level in the workspace, namely from `ros_ws` (or whatever your workspace is called).

Packages can be located in any subfolder within `src`.  For this class, packages will be created within each lab folder.  

Each package is contained in a folder with the name of the package.  Within this folder is another folder with the same name, and in that folder is the Python code defining each of the nodes.  Packages also contain three key configuration files: `package.xml`, `setup.cfg` and `setup.py`.  The next section will describe how to edit these to configure a package.

___
# 2. Create ROS Packages

As mentioned above, ROS packages must be created in the `src` folder of the workspace or a subfolder of `src`.  ROS documentation provides two tutorials on Python packages, and we will explore them here.

## A Bare Minimum Package

A minimum package is introduced in the [Creating a package](https://docs.ros.org/en/humble/Tutorials/Beginner-Client-Libraries/Creating-Your-First-ROS2-Package.html) tutorial.  Step through the Python version of this tutorial for an overview of what is contained in a ROS package and how to create one.  But make this one change: the location of your package should be inside your `<student_repo>/lab3_nodes` folder.  

## A Package with Publisher and Subscriber Nodes

Next, step through the Python [Publisher and Subscriber](https://docs.ros.org/en/humble/Tutorials/Beginner-Client-Libraries/Writing-A-Simple-Py-Publisher-And-Subscriber.html) tutorial.  Again, the package location should be inside your `<student_repo>/lab3_nodes` folder.  Here the publisher is defined by a Python class and similarly the subscriber is defined with a Python class.  These Python scripts provide templates for how to create ROS nodes; notice how classes are defined that inherit node properties and specify the action of the node such as to publish or subscribe to a topic.  

___
### [Back to Index](../Readme.md)
