# Turtlebots

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)
## [Index](../Readme.md)
## Contents

* [Turtlebots as Mobile Platforms](#turtlebots-as-mobile-platforms)
* [Caring for your Turtlebot](#caring-for-your-turtlebot)
  * [Remember these 2 Points](#remember-these-2-points)
  * [Battery and Power](#battery-and-power)
* [WiFi Network](#wifi-network)
* [Connecting Turtlebots to a Network](#connecting-turtlebots-to-a-network)
  * [Set the `ROS_DOMAIN_ID`](#set-the-ros_domain_id)
* [Onboard Operation of the Turtlebot](#onboard-operation-of-the-turtlebot)
* [Remote Operation of the Turtlebot](#remote-operation-of-the-turtlebot)
* [IP4 Subnets](#ip4-subnets)
* [Borrowing and Returning Turtlebots](#borrowing-and-returning-turtlebots)


# Turtlebots as Mobile Platforms

There is a good introduction and full documentation on Turtlebots here: [https://emanual.robotis.com/docs/en/platform/turtlebot3/overview/#overview](https://emanual.robotis.com/docs/en/platform/turtlebot3/overview/#overview)  They provide a convenient autonomous vehicle simulator.  Compared to a full-sized AV, they are inexpensive, safe to operate and much less complex.  Nevertheless, navigation tasks with them share many similarities and challenges with a full autonomous vehicle.  One still needs to do perception, path planning and control.  

We have already explored use of simulated [Turtlebots in Gazebo](Gazebo_Simulator.md).  This document focuses on use of real Turtlebots.  The key thing to note is that you should be able to write and run exactly the same ROS code to control a real Turtlebot and a simulated Turtlebot.  There are some physical care requirements of the real Turtlebots and networking requirements that differ.  


# Caring for your Turtlebot

The Turtlebots are battery powered, and operate for between 30 and 60 minutes on a full charge.  When turned off, they will gradually drain the battery, and will destroy the battery if left to completely drain.  So it is essential that if you are not going to use the Turtlebot for more than a few days that you disconnect the battery.  That is done by pulling appart the connector shown below:

![Disconnect the battery](.Images/turtlebotBattery.png)

Turtlebots can move suddenly and unpredictably.  A fall from a table is likely to damage or destroy the Lidar and other parts.  Thus, even though it is more convenient to observe them on a tabletop, always run them on the ground, not on a tabletop.  

## Remember these 2 points
1. Always run your Turtlebot on the floor, **not on a table-top.**
2. When you turn off the Turtlebot, **also disconnect the battery**, as shown above.

## Battery and Power

Normal operation is to connect the battery to provide power.  Alternatively, you can unplug the battery and directly plug the 5V power supply into the black jack on the Raspberry Pi.  This is convenient for when you are using the Turtlebot without driving it.  The battery can be charged by connecting it to the blue battery connector, which in turn is connected to the power supply.  Also, Brian will be able to charge your Turtlebot overnight if you return it to him. 


# WiFi Network

At MSU the Turtlebots will be configured to use the *turtlebot* engineering network as this unblocks the ports needed by ROS which are blocked by the standard engineering network.  

At home, network configuration is simple.  Attached a keyboard and mouse via USB, and a monitor via HDMI, to the Raspberry Pi board on the Turtlebot.  Turn on the Turtlebot and the Raspiberry Pi OS will come up with a GUI.  From this you can select your home network and enter the password.  

You will need the IP address of your Turtlebot.  If you have a monitor and keyboard already connected, then open a terminal window and use `hostname -I` to find the IP address and note it down.  Alternatively, some of the Turtlebots have a LCD screen that shows the IP address, as shown below:

![Turtlebot LCD](.Images/turtlebotLCD.png)

Once you have the IP address, you can connect to the Turtlebot from another computer on the network with `ssh`:
```bash
$ ssh ubuntu@<turtlebot_ip_address>
```
The password will be provided on Piazza.

On the engineering network, you should not need the IP address and instead you can use the Turtlebot name.  For example, if your Turtlebot is named: `turtlebot07`, like the one above, connect to it with:
```
$ ssh ubuntu@turtlebot07.egr.msu.edu
```
And enter the same password.

# Connecting Turtlebots to a Network

In order for ROS to communicate between your laptop and/or desktop and the Turtlebot, they must be on the same subnet.  In practice, this means connected to the same WiFi network.  The first 3 numbers of 4 in the IP4 address should be the same -- only the last number should differ.

We need to edit the network config file in the turtlebot. The way we do it is by `ssh`(secure shell) ing in to the turtlebot.
Follow the commands from the previous section to `ssh` into the turtlebot when you are on the MSU netowork, or by carefully connecting a monitor and a keyboard to the raspberry pi on the turtlebot.
Run the command below once you are in the turtlebot's shell.
```
sudo nano /etc/netplan/50-cloud-init.yaml
```
In the above command you are trying to open the file with `sudo`(super user do) permisson. sudo is used to run commands with admin privilages. `nano` is one of the many popular command line editors. 

You should be able to see the file with network details of EGR Wi-Fi. Similarly, add details of your Wi-Fi network as shown below.
```

# This file is generated from information provided by the datasource.  Changes
# to it will not persist across an instance reboot.  To disable cloud-init's
# network configuration capabilities, write a file
# /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg with the following:
# network: {config: disabled}
network:
  version: 2
  renderer: networkd
  ethernets:
    eth0:
      dhcp4: true
      dhcp6: true
      optional: true
  wifis:
    wlan0:
      dhcp4: true
      dhcp6: true
      access-points:
        turtlebot:
          password: rosengineering

# these are the lines you will be adding to the .yaml file          
    wlan0:
       dhcp4: true
       dhcp6: true
       access-points:
         <your_wifi_name>:
          password: <wifi_password>

```

Editing files in the command line could be tricky, be careful with the indendation, as that could cause errors. 
Once you're done editing the file, hit `Ctrl+X` and then `Y`

### Testing the connection
Once the above steps are done, you should restart the turtlebot and power it up. The way you do it is by using the command.
The device should turn off with the below command 
```
sudo shutdown now
```
This is preferable to turning off the Turblebot with a switch.  Once it is successfully turned off, turn off the turtlebot and turn it back on. Give it a minute and to startup and you should be able to `ssh` into the turtlebot from your home network.

## Set the `ROS_DOMAIN_ID`

Before using your Turtlebot, select a `ROS_DOMAIN_ID`.  Choose one of the numbers assigned to one of your team members.  Then edit the `.bashrc` file and set this number instead of the default `0` value.  Whenever you make changes to the `.bashrc` file, source it with:
```
$ source ~/.bashrc
```

# Onboard Operation of the Turtlebot

In this section, it is assumed you are connected via ssh to the Turtlebot.  You can start up the ROS packages on the Turtlebot with:
```
$ bringup
```
This should start a sequence of ROS nodes without any errors.  Leave the nodes running in the Terminal.

Now open another `ssh` session on your Turtlebot.  Have a look at what the `bringup` command did by typing:
```
$ alias bringup
```
You will see that `bringup` is an alias for the command:
```
ros2 launch turtlebot3_bringup robot.launch.py
```
To start up your Turtlebot you can either type this full command or the `bringup` shortcut.  If your Turtlebot ROS nodes are already running, no need to do it again.

Let's make sure the Turtlebot is publishing topics.  In this second `ssh` terminal, have a look at the topics with:
```
$ ros2 topic list
```

You should see the `/cmd_vel` topic.  The Turtlebot is subscribing to this topic, and we can control it by publishing  a Twist on this topic, just like we did for the Turtlebot in Gazebo.   **Make sure your Turtlebot is on the floor.** Then command it to move in a circle with:
```
ros2 topic pub /cmd_vel geometry_msgs/msg/Twist '{linear: {x: 0.2}, angular: {z: -0.2}}' -1
```
Stop it with:
```
ros2 topic pub /cmd_vel geometry_msgs/msg/Twist '{}' -1
```

# Remote Operation of the Turtlebot

You will need ROS running on your laptop or desktop, which must be on the same WiFi network as your Turtlebot.  It is recommended that you follow the [directions to set up VirtualBox](../Setup/VirtualBox.md) on your computer.  This will give you Ubuntu with ROS pre-installed.  If you prefer you can install ROS directly on your computer, but you will need to figure this out yourself.  Note, that running ROS in a WSL instance will not work, as it will not be able to access the same subnet as the host computer.

Let's confirm that ROS can communicate over your network between your laptop or desktop and the Turtlebot.  This assumes you have started your Turtlebot ROS package, as explained [above](#running-a-real-turtlebot).  Then, in a terminal on your computer (meaning in your VirtualBox unless you installed ROS directly), source the overlay.  This will bring up ROS.  Now see if you can see the topics being published by your Turtlebot using
```
ros2 topic list
```
You should see all the above ROS topics.  

Once this works, then confirm that you can control the Turtlebot from your Laptop by publishing the above Twist commands.  

Remote operation of the turtlebot is important for a couple reasons.  In part it is useful for enabling remote visualization of the Turtlebot as the maps it builds.  Also, some tasks are too computationally expensive to perform on the Turtlebot and are better performed remotely.  We'll be exploring doing SLAM on for the Turtlebot.  The Turtlebot handles data collection and wheel actuation, while the map building and visualization are performed remotely. 

# IP4 Subnets

ROS will automatically discover and communicate with ROS nodes that have the same `ROS_DOMAIN_ID` **and that are on the same subnet**.  How can you tell if your computer or VirtualBox is on the same subnet as, say, your Turtlebot?  Have a look at your IP4 address, which will be in the format `W.X.Y.Z`, where the letters indicate numbers in the range `0` to `255`.  Typically, if computers have IP addresses with the first 3 numbers the same, i.e. the same `W.X.Y`, then they are on the same subnet. 

Now, the Turtlebots are registered on the `EGR Wi-Fi WPA2` WiFi, so make sure your laptop is also connected to this WiFi, or else it will definitely be on a different subnet.  However, even if you are on the same WiFi, it turns out that Engineering assigns computers to a variety of different subnets.  That means if you are lucky, your laptop will be assigned to the same subnet as your Turtlebot (which you can check by comparing the `W.X.Y` numbers), and you'll be able to communicate.  Otherwise, you are out of luck.  We are working with DECS to address this and will hopefully have a solution by Thursday 10/27/2022.  

# Borrowing and Returning Turtlebots

Your team is permitted to take your Turtlebot to your dorm room.  But take good care of them as they are expensive.  You can return them at any time during regular office hours, either by dropping it off at the ECE main office 2120 EB, or taking it to Brian Wright's office: 3234 EB.    You can re-borrow it from Brian by making arrangements to pick it up, or else picking it up during lab hours.  His contact is: 517-355-5233, wrightb@egr.msu.edu.  

Note that each team will be assigned a particular Turtlebot with a numbered ID (that is different from its `ROS_DOMAIN_ID`), and you will keep using that same Turtlebot.  Do **not** use a Turtlebot from another team.

___
### [Back to Index](../Readme.md)
