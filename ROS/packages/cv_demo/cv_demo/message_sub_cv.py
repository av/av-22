#!/usr/bin/env python
'''
    message_sub_cv.py 

    This is a ROS node that subscribes to a topic and displays messages in a rotating window
    This has two callbacks: one to read the message from the topic and one to display the message
    on the screen.  These can run at separate rates.
    Notice the need to use locks to pass data between these callbacks.
    All OpenCV calls are within just one callback (and so in one thread)

    Daniel Morris, 2020, 2022
'''
import rclpy
from rclpy.node import Node
from std_msgs.msg import String
from .image_rotator import ImageRotator
from threading import Lock

class MessageDisplay(Node):

    def __init__(self, topic_name='text_message'):        
        super().__init__('message_sub')
        self.message_changed = False
        self.message_lock = Lock()     # A lock to keep the message safe from multiple threads
        self.imr = None                # Allocate variable for ImageRotator, but don't initialize until in display
        self.subscription = self.create_subscription(String, topic_name, self.read_message, 1)
        self.subscription 
        self.get_logger().info('Subscribing to topic: ' + topic_name)
        self.timer = self.create_timer(0.05, self.display)     # Display 20 times per second

    def read_message(self, msg):
        ''' This just reads published messages
        '''
        with self.message_lock:           # Keep following block thread-safe
            self.message = msg.data       # Message only copied when safe
            self.message_changed = True   # This variable also kept thread-safe

    def display(self):
        ''' This displays the message in a rotating window 
            All OpenCV calls are in this thread
        '''
        if self.imr is None:
            self.imr = ImageRotator()     # This class handles OpenCV plotting functions

        with self.message_lock:            
            if self.message_changed:      # If new message, re-create the image
                self.imr.create_image_from_message(self.message)
                self.message_changed = False
                
        keep_going = self.imr.show_rotated_image( add_deg=1 )  # Rotate by 1 degree
        if not keep_going:
            raise SystemExit  # If user pressed "q"

def main(args=None):
    rclpy.init(args=args)

    node = MessageDisplay()
    try:
        rclpy.spin(node)       
    except SystemExit:
        node.destroy_node()
        rclpy.shutdown()
    except KeyboardInterrupt:
        pass
