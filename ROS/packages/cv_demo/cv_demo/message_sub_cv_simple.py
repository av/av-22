'''
    message_sub_cv_simple.py 

    This is a ROS node that subscribes to a topic and displays the message in an OpenCV window
    Note that a single callback handles both message reading and display.  This is a simple
    solution, but means that the display window is only updated when a new message arrives.
    All OpenCV calls are within just one callback (and so in one thread)
    
    Daniel Morris, 2020, 2022
'''
import rclpy
from rclpy.node import Node
from std_msgs.msg import String
from .image_rotator import ImageRotator

class MessageDisplay(Node):

    def __init__(self, topic_name='text_message'):        
        super().__init__('message_simple_sub')
        self.imr = None          # Allocate variable for ImageRotator, but don't initialize until in subscriber
        self.subscription = self.create_subscription(String, topic_name, self.read_message_and_display, 1)
        self.subscription 
        self.get_logger().info('Subscribing to topic: ' + topic_name)

    def read_message_and_display(self, msg):
        ''' Reads published message and displays it in a rotating window
            All OpenCV calls are in this thread
        '''
        if self.imr is None:
            self.imr = ImageRotator()      # Initialize in callback since this class handles OpenCV plotting functions

        self.imr.create_image_from_message( msg.data )
        keep_going = self.imr.show_rotated_image( add_deg=20 )
        if not keep_going:
            raise SystemExit  # If user pressed "q"

def main(args=None):
    rclpy.init(args=args)

    node = MessageDisplay()
    try:
        rclpy.spin(node)
    except SystemExit:
        node.destroy_node()
        rclpy.shutdown()        
    except KeyboardInterrupt:
        pass
