from setuptools import setup

package_name = 'cv_demo'

setup(
    name=package_name,
    version='0.9.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Daniel Morris',
    maintainer_email='dmorris@msu.edu',
    description='OpenCV Demo',
    license='Apache-2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'message_pub = cv_demo.message_pub:main',
            'message_sub_cv_simple = cv_demo.message_sub_cv_simple:main',
            'message_sub_cv = cv_demo.message_sub_cv:main',
        ],
    },
)
