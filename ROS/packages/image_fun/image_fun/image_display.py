#!/usr/bin/env python
'''
    image_display.py 

    This is an exmple ROS node for subscribing to image topics and displaying images
    Can specify the image topic name as well as whether or not it is compressed
      Usage:
    ros2 run image_fun image_display <topic_name> 
    ros2 run image_fun image_display <topic_name> --compressed

    Use the optional --compressed argument if the image topic is type compressed
    Use --format <fmt> to specify and alternate to the default bgr8 image format

    Daniel Morris, Nov 2020, 2022
'''
import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image, CompressedImage
import argparse
import cv2 as cv
from cv_bridge import CvBridge, CvBridgeError

class ImageDisplay(Node):
    def __init__(self, topic_name, compressed, fmt):
        super().__init__('image_display')        
        self.compressed = compressed
        self.fmt = fmt
        self.title = f'{topic_name}, type: compressed' if compressed else f'{topic_name}, type: raw'
        self.bridge = CvBridge()
        self.get_logger().info(f'Subscribed to: {self.title}')
        if self.compressed:
            self.subscription = self.create_subscription(CompressedImage, topic_name, self.image_cb, 1)
        else:
            self.subscription = self.create_subscription(Image, topic_name, self.image_cb, 1)
        self.subscription 
        
    def image_cb( self, msg ):
        if self.compressed:
            img = self.bridge.compressed_imgmsg_to_cv2(msg, self.fmt)
        else:
            img = self.bridge.imgmsg_to_cv2(msg, self.fmt)
        cv.imshow(self.title, img )
        if cv.waitKey(1) & 0xFF == ord('q'):
            raise SystemExit  # If user pressed "q"           

def main(args=None):
    rclpy.init(args=args)

    parser = argparse.ArgumentParser(description='Image type arguments')
    parser.add_argument('topic_name',      default="/image",     type=str, help="Image topic to subscribe to")
    parser.add_argument('--compressed',    action='store_true',            help='Set if compressed image message')
    parser.add_argument('--format',        default="bgr8",       type=str, help="Image format")
    args, unknown = parser.parse_known_args()
    if unknown: print('Unknown args:',unknown)

    node = ImageDisplay( topic_name=args.topic_name, compressed=args.compressed, fmt=args.format)  
    try:
        rclpy.spin(node)       
    except SystemExit:
        node.destroy_node()
        rclpy.shutdown()
    except KeyboardInterrupt:
        pass
