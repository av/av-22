#!/usr/bin/env python
''' Minimial Publisher Tells Secrets
'''
import rclpy
from rclpy.node import Node
from std_msgs.msg import String

class Teller(Node):
    def __init__(self, topic_name='secret'):
        super().__init__('secret_teller')
        self.publisher_ = self.create_publisher(String, topic_name, 1)
        
    def run(self):
        secret = String()
        while not secret.data.lower() == 'quit':
            secret.data = input('Input secret: ')
            self.publisher_.publish( secret )
        
def main(args=None):
    rclpy.init(args=args)
    tell = Teller()
    tell.run()
