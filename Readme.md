**This is an old version of the notes.  See the most recent here: https://gitlab.msu.edu/av/autonomous-vehicles**

# ECE-CSE 434 Autonomous Vehicles Course

Notes for: **Fall Semester, 2022**
___
### Instructor and author: [Daniel Morris](https://www.egr.msu.edu/~dmorris), PhD 

Copyright (C) 2020 - 2022
___
## Content
0. [Introduction](#0-introduction)
1. [Setup Your Environment](#1-setup-your-environment)
2. [Python Review](#2-python-review)
3. [ROS 2](#3-ros-2)
4. [AV Topics](#4-av-topics)
___
# 0. Introduction

The documents in this repository are class notes for the **Build** portion of *ECE-CSE 434 Autonomous Vehicles*.  They will guide you through learning ROS as well as implementing some key topics in Autonomous Vehicles.  The style is hands-on, and intended for you to be following along and entering the commands onto your own computer.  I believe you'll learn best if you try out the examples as you read them.

In addition to being a tutorial, these documents are also a troubleshooting reference.  For beginners it is easy to forget or skip important steps when building or running ROS packages.  If something is not working that you think should be working, head to the appropriate section and see what you did differently.  

# 1. Setup Your Environment

The following are installation instructions to get the software you need for the class.

* **[1.1. Installation](Setup/Installation.md)**:  Options to install ROS 2
* **[1.2. Shell Configuration](Setup/Shell_Config.md)**:  Configure a shell for ROS 2
* **[1.3. Python Environment](Setup/Python_Environment.md)**: Configure Python to use the ROS environment and libraries such as OpenCV
* **[1.4. VSCode](Setup/VSCode.md)**: My preferred IDE for Python and ROS code.
* **[1.5. VSCode in HPCC](Setup/HPCC_VSCode.md)**: Use VSCode to remotely edit files in HPCC.
* **[1.6. HPCC ROS](Setup/HPCC_ROS.md)**: Configure HPCC to run ROS using a Docker Image.
* **[1.7. Assignments and Git](Setup/Assignments_Git.md)**: Basics for using Git in this class, including receiving and submitting assignments
* **[1.8. VirtualBox](Setup/VirtualBox.md)**: Install VirtualBox containing Ubuntu and ROS on your laptop or desktop.

___
# 2. Python Review

* **[2.1 Python Notes Repo](https://github.com/dmorris0/python_intro/blob/main/README.md)**
  * A quick overview of Python needed for this class.

___
# 3. ROS 2

* **[3.1. Start Programming with ROS 2](ROS/ROS_Packages.md)** 
  * [ROS 2 Prerequisites](ROS/ROS_Packages.md#1-ros-2-prerequisites)
  * [Create ROS Packages](ROS/ROS_Packages.md#2-create-ros-packages)
* **[3.2. Gazebo Simulator](ROS/Gazebo_Simulator.md)**
  * [Introduction](ROS/Gazebo_Simulator.md#introduction)
  * [Bring Up Gazebo](ROS/Gazebo_Simulator.md#bring-up-gazebo)
  * [Turtlebot Topics and Control](ROS/Gazebo_Simulator.md#turtlebot-topics-and-control)
  * [Gazebo World Modifications](ROS/Gazebo_Simulator.md#gazebo-world-modifications)
  * [Troubleshoot Gazebo](ROS/Gazebo_Simulator.md#troubleshoot-gazebo)
  * [RViz](ROS/Gazebo_Simulator.md#rviz)
* **[3.3. ROS Messages, Topics and Rosbags](ROS/Messaging.md)**
  * [ROS Messages](ROS/Messaging.md#ros-messages)
  * [ROS Topics and Rosbags](ROS/Messaging.md#ros-topics-and-rosbags)
* **[3.4 Multiple Threads in ROS and OpenCV](ROS/Multiple_Threads_OpenCV.md)**
  * [Mutli-Threaded Nodes](ROS/Multiple_Threads_OpenCV.md#multi-threaded-nodes)
  * [Example 1: Subscribe to an Image Topic](ROS/Multiple_Threads_OpenCV.md#example-1-subscribe-to-an-image-topic)
  * [Example 2: A Callback with OpenCV](ROS/Multiple_Threads_OpenCV.md#example-2-a-callback-with-opencv)
  * [Python Locks](ROS/Multiple_Threads_OpenCV.md#python-locks)
  * [Example 3: Two Callbacks with Locks](ROS/Multiple_Threads_OpenCV.md#example-3-two-callbacks-with-locks)
* **[3.5 Turtlebots](ROS/Turtlebot.md)**
  * [Turtlebots as Mobile Platforms](ROS/Turtlebot.md#turtlebots-as-mobile-platforms)
  * [Caring for your Turtlebot](ROS/Turtlebot.md#caring-for-your-turtlebot)
  * [WiFi Network](ROS/Turtlebot.md#wifi-network)
  * [Connecting Turtlebots to a Network](ROS/Turtlebot.md#connecting-turtlebots-to-a-network)
  * [Onboard Operation of the Turtlebot](ROS/Turtlebot.md#onboard-operation-of-the-turtlebot)
  * [Remote Operation of the Turtlebot](ROS/Turtlebot.md#remote-operation-of-the-turtlebot)
  * [IP4 Subnets](ROS/Turtlebot.md#ip4-subnets)
  * [Borrowing and Returning Turtlebots](ROS/Turtlebot.md#borrowing-and-returning-turtlebots)

___
# 4. AV Topics

* **[4.1. Logistic Regression](AV/Logistic_regression.md)**
  * [Introduction](AV/Logistic_regression.md#introduction)
  * [Targets and Clutter](AV/Logistic_regression.md#targets-and-clutter)
  * [Linear Classifiers](AV/Logistic_regression.md#linear-classifiers)
  * [Color-Based Target Classification](AV/Logistic_regression.md#color-based-target-classification)
  * [Logistic Regression Results](AV/Logistic_regression.md#logistic-regression-results)
  * [Code Explanation](AV/Logistic_regression.md#code-explanation)
  * [Can Detection Be Improved?](AV/Logistic_regression.md#can-detection-be-improved)
* **[4.2. Camera Calibration](AV/camera_calibration.md)**
  * [Introduction](AV/camera_calibration.md#introduction)
  * [Checkerboard Image Data](AV/camera_calibration.md#checkerboard-image-data)
  * [Uncompress Images](AV/camera_calibration.md#uncompress-the-image-topic)
  * [Intrinsic Calibration](AV/camera_calibration.md#do-intrinsic-calibration)
  * [Fix the `/tmp` Folder Issue](AV/camera_calibration.md##fix-the-tmp-folder-issue)
* **[4.3 SLAM](AV/SLAM.md)**
  * [Cartophrapher SLAM](AV/SLAM.md#cartographer-slam)
  * [SLAM on a Real Turtlebot](AV/SLAM.md#slam-on-a-real-turtlebot)
  * [SLAM on a Simulated Turtlebot](AV/SLAM.md#slam-on-a-simulated-turtlebot)
  * [Save the Map](AV/SLAM.md#save-the-map)
* **[4.4 PID Controller](AV/PID.md)**
  * [PID Controller](AV/PID.md#pid-controller)
  * [Feedback](AV/PID.md#feedback)
  * [State Modeling](AV/PID.md#state-modeling)
  * [PID Code Example](AV/PID.md#pid-code-example)
  * [Exercises](AV/PID.md#exercises)
  * [Line Follower](AV/PID.md#line-follower)  
   

More notes will be added here during the semester
