# Assignments and Git

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)
### [Index](../Readme.md)
## Content
* [Git Basics](#git-basics)
  * [Cloning Using Tokens](#cloning-using-tokens)
  * [Git in VSCode](#git-in-vscode)
* [Lab Assignments](#lab-assignments)
___
# Git Basics

Git is an important tool for this course.  You will be using it to download class notes and assignments, as well as to upload your completed labs to the MST Gitlab server.  It is a powerful source management tool with extensive documentation available.  However, for this class, only a basic knowledge of it is necessary.  This document will get you started with Git if you are not familiar with it.

First, I recommend reading the [Gitlab basics](https://gitlab.msu.edu/help/gitlab-basics/start-using-git.md) documentation on Gitlab.

There are a few things to be aware of for this course:

* I recommend cloning with the `https` option rather than `ssh`.  This is because MSU blocks ssh access to most computers (except a few gateways) from outside the university.  
* If you wish to use multiple branches you may.  However, when you submit your assignments it must always be merged to the `main` branch as this is what the grader will pull.  The easiest thing if you are not familiar with branches is only to use the `main` branch.  

## Cloning Using Tokens

Gitlab requires tokens rather than passwords when you clone a private repository.  You can create your own tokens as described here: [https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).  Then to clone a repo with your token you type the following in the command line:
```
git clone https://<username>:<token>@gitlab.msu.edu/<repo>
```
Here `<username>` is your user name, `<token>` is the token you just created, and `<repo>` is the `.git` repo you are cloning.

## Git in VSCode
It turns out that VSCode integrates very nicely with Git.  You can commit, push, pull all from the VSCode interface without needing to use a terminal.  Once you figure out how to use this interface, you won't want to use Git any other way.  Instructions are here: [https://code.visualstudio.com/docs/editor/versioncontrol](https://code.visualstudio.com/docs/editor/versioncontrol).


___
# Lab Assignments

Instructions on obtaining and submitting lab assignments are here: [https://gitlab.msu.edu/labs/labs_2022](https://gitlab.msu.edu/labs/labs_2022)

You must be enrolled in the class to access this folder, and you will need to clone it using a token, as described above. When the instructor announces that a lab assignment is released, the assignment will be added to this repo, and you can obtain it by syncing the repo.   

___
### [Back to Index](../Readme.md)
