# List of packages to install to go along with foxy
# In part from: https://docs.ros.org/en/foxy/Installation/Ubuntu-Install-Debians.html
# Also from foxy_noetic.Dockerfile
# Note: make sure to do to repository setup *without* a VPN
# Also, after installing the below, the VirtualBox vdi disk takes up 14.5 GB
#

locale  # check for UTF-8

sudo apt update && sudo apt install locales
sudo locale-gen en_US en_US.UTF-8
sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
export LANG=en_US.UTF-8

locale  # verify settings

sudo apt update && sudo apt install -y curl gnupg2 lsb-release
sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key  -o /usr/share/keyrings/ros-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(source /etc/os-release && echo $UBUNTU_CODENAME) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null

sudo apt update

sudo apt upgrade

sudo apt install -y ros-foxy-desktop

sudo apt install -y net-tools openssh-server python3-pip \
    python3-tk git python3.8-venv

sudo apt install -y ros-foxy-gazebo-* \
    ros-foxy-cartographer ros-foxy-cartographer-ros \
    ros-foxy-navigation2 ros-foxy-nav2-bringup

sudo apt-get install -y ros-foxy-dynamixel-sdk \
    ros-foxy-turtlebot3-msgs ros-foxy-turtlebot3 ros-foxy-turtlebot3-simulations

sudo apt-get install -y ros-foxy-camera-calibration-parsers ros-foxy-camera-info-manager \
    ros-foxy-launch-testing-ament-cmake ros-foxy-usb-cam \
    ros-foxy-image-transport-plugins ros-foxy-image-pipeline

sudo apt-get install -y ros-foxy-turtle-tf2-py \
    ros-foxy-tf2-tools ros-foxy-tf-transformations \
    wget curl libopencv-dev python3-opencv


