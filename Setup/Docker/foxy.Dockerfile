#  foxy.Dockerfile
#
#  Docker for a customized foxy ROS instance
#  Creates a local user called avc
#  Mounts folders from the host machine to /mnt/code and /mnt/data, and links these to the home folder
#    The idea is to store all src files on the local machine so they are not lost when the docker is removed
#    You can create a sumbolic link to them from within your home folder
#    These are not needed when Docker is used in a Singularity
# 
#  Daniel Morris, Sep 2022
#
# Here are various Docker commands I used to create the Docker image, and to run it. Note: it is recommended to do 
# these commands in Ubuntu in WSL, rather than directly in PowerShell:
#  docker login
#  docker build -t morris2001/foxy -f foxy.Dockerfile .
#  docker push morris2001/foxy
# To run it, give it whatever name you like.  I chose foxy:
#  docker run -it -v /home/dmorris/av:/mnt/av -v /home/dmorris/data:/mnt/data -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --network host --name foxy morris2001/foxy
# options explained
#  -it                                      create interactive docker
#  -v /home/dmorris/av:/mnt/av              mount a unix drive in the docker so we can store code or data on unix host (applies to Docker in WSL)
#  -v c:/Users/<path_to_av_folder>:/mnt/av  OR if running directly on Windows (not WSL), can link to a folder on the C drive this way 
#  -e DISPLAY=$DISPLAY                      Needed to display GUI from Docker
#  -v /tmp/.X11-unix:/tmp/.X11-unix         Needed to display GUI from Docker
#  --network host                           Will share host network to communicate with Turtlebot (not the most secure way to do this)
#  --name foxy                             Name with which to refer to the downloaded image (so can stop and start it)
#  morris2001/foxy                          This is the online Docker image created by the instructor
#
#  docker run -it --network host --name foxywin morris2001/foxy
#
# To open an additional shell in this docker container when it is running:
#  docker exec -it foxy bash
# When you exit all your foxy containers, it will stop.  You can see all your containers and their
# status with the command:
#  docker ps -a
# To start it do:
#  docker start foxy
# And then use the above "docker exec ..." command to open a shell in this container.  It should have
# kept any changes to your filesytem.
# If you get a new foxy image and want to run it as foxy, then you'll need to delete
# the current foxy container with:
#  docker stop foxy
#  docker rm foxy
# Note: this will delete all the changes you made to the filesystem.  That is why we keep our code and data 
# on the host filesystem and just create symbolic links to them.
FROM osrf/ros:foxy-desktop

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

ENV ROS_DISTRO foxy

# Key development tools:
RUN apt-get update && \
    apt-get install -y python3-pip python3-tk git python3.8-venv && \
    rm -rf /bar/lib/apt/lists/*

# ROS Foxy tools including Gazebo, Cartographer and Navigation2
RUN apt-get update && \
    apt-get install -y ros-foxy-gazebo-* \
    ros-foxy-cartographer ros-foxy-cartographer-ros \
    ros-foxy-navigation2 ros-foxy-nav2-bringup && \
    rm -rf /bar/lib/apt/lists/*

# Turtlebot3 packages
RUN apt-get update && \
    apt-get install -y ros-foxy-dynamixel-sdk \
    ros-foxy-turtlebot3-msgs ros-foxy-turtlebot3 ros-foxy-turtlebot3-simulations && \
    rm -rf /bar/lib/apt/lists/*


# Add basic user
ARG USERNAME=avc
ARG USERID=1001

ENV USERNAME ${USERNAME}
ENV USERID ${USERID}

RUN useradd -m $USERNAME && \
        echo "$USERNAME:$USERNAME" | chpasswd && \
        usermod --shell /bin/bash $USERNAME && \
        usermod -aG dialout,sudo $USERNAME && \
        echo "$USERNAME ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers && \
        chmod 0440 /etc/sudoers && \
        usermod  --uid $USERID $USERNAME

# Change user
USER $USERNAME

# Augment the .bashrc file:
# Since this docker only ever runs ROS foxy, let's source the underlay
RUN echo 'source /opt/ros/${ROS_DISTRO}/setup.bash' >> $HOME/.bashrc && \
    echo 'export TURTLEBOT3_MODEL=burger' >> $HOME/.bashrc && \
    echo 'cd $HOME' >> $HOME/.bashrc

# Initialize rosdep:
RUN /bin/bash -c 'source /opt/ros/${ROS_DISTRO}/setup.bash; rosdep update' 

# Set up python virtual environment:
RUN mkdir -p $HOME/av/venvs && python3 -m venv --system-site-packages $HOME/av/venvs/av && \
    echo 'alias act="source $HOME/av/venvs/av/bin/activate"' >> $HOME/.bashrc

