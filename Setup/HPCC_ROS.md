# ROS in Docker on HPCC

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)
### [Index](../Readme.md)
## Content
* [Introduction](#introduction)
* [Configure Your HPCC Environment](#configure-your-hpcc-environment)
  * [Configure CentOS](#configure-centos)
  * [Configure Ubuntu](#configure-ubuntu)
* [Running ROS on HPCC](#running-ros-on-hpcc)
  * [Start and Ubuntu Shell](#start-an-ubuntu-shell)
  * [Test Your ROS Environment](#test-your-ros-environment)
* [Create a Link to Class Data](#create-a-link-to-class-data)
* [Interactive Desktop](#interactive-desktop)
  * [HPCC On-Demand](#hpcc-on-demand)
  * [ROS in an Interactive Desktop](#ros-in-an-interactive-desktop)

___
# Introduction

Everyone should have received an email telling them that they have been given an HPCC accout. There is good documentation on the HPCC including:
* [Main documentation page](https://docs.icer.msu.edu/).  Please consult and search this for questions first.
* [Web access to HPCC](https://ondemand.hpcc.msu.edu) 
* [Submit a help request](https://contact.icer.msu.edu/contact).  The ICER staff are very helpful and usually respond within a few hours or a day.

This document will explain how to get ROS going on your HPCC environment.

___
# Configure Your HPCC Environment

To configure your HPCC environment, first open a remote VSCode instance as explained in [HPCC VSCode](HPCC_VSCode.md).  Then use VSCode to create startup scripts as follows:

## Configure CentOS

The HPCC system runs a variant of Linux called *CentOS*.  To add a couple commands for pulling and running a docker container, use VSCode to add the following lines to the bottom of your `.bashrc` file, which is located in your home folder, namely: `/mnt/home/<user_name>/.bashrc` or simply `~/.bashrc`.  **Note: the following lines pull an updated Docker image that includes both ROS2 Foxy and ROS 1 Noetic.  Replace the earlier `alias` lines in your `.bashrc` file with the commands below.**
```
# Command prompt show current path:
PS1="\w\$ " # Setting PS1 to show full path
export PROMPT_DIRTRIM=4 # Trimming path to 4 directories

# ROS initialization
alias docker_pull='singularity pull ~/foxy_noetic.sif docker://morris2001/foxy_noetic:latest'
alias docker_shell='singularity exec ~/foxy_noetic.sif /bin/bash'
# Rest of ROS initialization in .rosinit  
# After typing: docker_shell
# then type: source ~/.rosinit
alias humble_pull='singularity pull ~/humble_latest.sif docker://morris2001/humble:latest'
alias humble_shell='singularity exec ~/humble_latest.sif /bin/bash'
```
Save the modified `.bashrc` file, and if you have any terminals open, you can close them or else source your `.bashrc` file in each with:
```
source ~/.bashrc
```
## Configure Ubuntu

The Docker image containing both Ubuntu and ROS will be overlaid on top of your CentOS file system using a program called Singularity.  To configure your Ubuntu, create a new file called `.rosinit` in your home folder (i.e. `~/.rosinit`) and copy the following into it.  **Note: the following replaces the earlier version of the `.rosinit` file.  You'll need to update your `ROS_DOMAIN_ID`, `ROS_PORT` and `GAZEBO_PORT` numbers as before using your assigned ID.**
```
# ----------------------------------------------------------
# ROS config for ECE-CSE 434, Fall 2022
# from inside a singularity start this with:
#  source .rosinit
# Or:
#  source .rosinit noetic

# Deconflict ROS and Gazebo instances by adjusting the below:
export ROS_DOMAIN_ID=0             # replace 0 with your assigned number.  **No spaces around the "="
export ROS_PORT=51000              # replace 51000 with the sum of 51000 and your ROS_DOMAIN_ID
export GAZEBO_PORT=61000           # replace 61000 with the sum of 61000 and your ROS_DOMAIN_ID
export GAZEBO_MASTER_URI=http://localhost:$GAZEBO_PORT
export ROS_MASTER_URI=http://localhost:$ROS_PORT
echo "ROS_DOMAIN_ID: $ROS_DOMAIN_ID, ROS_PORT: $ROS_PORT, GAZEBO_PORT: $GAZEBO_PORT"

# Avoid compile warnings with colcon build:
export PYTHONWARNINGS=ignore:::setuptools.command.install,ignore:::setuptools.command.easy_install,ignore:::pkg_resources

if [ $# -eq 1 ] && [ "$1" == "noetic" ]
then 
  unset ROS_DISTRO
  echo "source /opt/ros/noetic/setup.bash"
  source /opt/ros/noetic/setup.bash 
  echo "source /opt/ros/foxy/setup.bash"
  source /opt/ros/foxy/setup.bash > /dev/null
  echo "** Both Noetic and Foxy are initialized."  
  echo "** Use this environment ONLY for playing ROS 1 bags"
  PS1="B:\w\$ " # Setting PS1 to show full path
  return
elif [ "$ROS_DISTRO" == "humble" ]
then
  echo "source /opt/ros/humble/setup.bash"
  source /opt/ros/humble/setup.bash
  PS1="H:\w\$ " # Setting PS1 to show full path  
else
  echo "source /opt/ros/foxy/setup.bash"
  source /opt/ros/foxy/setup.bash
  PS1="R:\w\$ " # Setting PS1 to show full path
fi

export PROMPT_DIRTRIM=4 # Trimming path to 4 directories

# Command to activate selected virtual environment:
VENV_FOLDER=$HOME/av/venvs                                     # Change this as appropriate
act() {
  if [ $# -eq 0 ] 
  then
    ls $VENV_FOLDER                       # If no arguments, display all virtual environments
  else
    cmd="source $VENV_FOLDER/${1}/bin/activate"   # Activate selected virtual environment
    echo $cmd
    eval $cmd
  fi
}
# The following will enable ROS nodes to access packages installed in a
# virtual environment.  Activating is insufficient for ROS Python to run it.
addpypath() {
  if [ $# -eq 0 ] 
  then
    ls $VENV_FOLDER                       # If no arguments, display all virtual environments
  else
    pyversion=`/usr/bin/ls $VENV_FOLDER/${1}/lib`
    newpath=$VENV_FOLDER/${1}/lib/$pyversion/site-packages
    echo 'Appending $PYTHONPATH:'"$newpath"
    cmd="export PYTHONPATH=$PYTHONPATH:$newpath"   # Activate selected virtual environment
    eval $cmd
  fi
}
# Command to set Turtlebot
# Usage:
#  tbot waffle
#  tbot burger
tbot() {
  if [ $# -eq 0 ] 
  then
    name='burger'          # If no arguments then assume burger
  else
    name=$1
  fi
  cmd='export TURTLEBOT3_MODEL='$name
  echo $cmd
  eval $cmd
}
# use default Turtlebot until changed by user
tbot
```
Notice 3 things with the above script:

1. There is a line that sets: `ROS_DOMAIN_ID=0`.  Edit this line and replace the `0` with the ID number given to you by your instructor.  This is important to avoid ROS message conflicts with others using HPCC cluster nodes.  In addition, to deconflict Gazebo with other students, update the `ROS_PORT` and `GAZEBO_PORT` variables with their raw value plus your `ROS_DOMAIN_ID` value.  

2. The above script creates an `act` function to activate a Python virtual environment.  If you have a virtual environment in your `$HOME/av/venvs` folder called `av`, then you can activate it with:
```
R:~$ act av
source /mnt/home/dmorris/av/venvs/av/bin/activate
(av) R:~$
```
This will let you install packages in this environment.  However, ROS will **not** be able to run these packages simply by activating the environment.  To enable ROS Python to use those packages, the `PYTHONPATH` environment variable needs to be set to include the virtual environment.  You can do that with the `addpypath` command defined above:
```
R:~$ addpypath av
Appending $PYTHONPATH:/mnt/home/dmorris/av/venvs/av/lib/python3.8/site-packages
```
Then ROS Python nodes will be able to access the packages you install in your virtual environment.  Note that there is not an easy way to remove this folder from your `PYTHONPATH` aside from exiting from your Docker shell.  

3. A `tbot` function is created for setting your Turtlebot model. By default it sets the Turtlebot to `burger` but you can change it to `waffle` with:
```
R:~$ tbot waffle
export TURTLEBOT3_MODEL=waffle
```

# Running ROS on HPCC

The above addition to your `.bashrc` file will make two Docker commands available when you have a shell open. Make sure you are in your home folder (typing the command `cd` will do this).  Then the first command is:
```
~$ docker_pull
```
Here the `~$` prefix indicated you are in your home folder, and it not something you type.  Execute this `docker_pull` just once and it will pull the ROS image created by the instructor for this course into your CentOS environment.  It will create a file called `foxy_noetic.sif` that stores this image.  Note that this ROS image was created by this [Docker File](Docker/foxy_noetic.Dockerfile).

## Start an Ubuntu Shell
The second command is `docker_shell`, which will use the `foxy_noetic.sif` image to convert the current CentOS shell into an Ubuntu shell. When you run this you'll see a line starting with `Singularity>`.  This is your new Ubuntu Docker image running under Singularity.  This needs to be initialized by sourcing your `.rosinit` file.  Here is what the commands will look like:
```
~$ docker_shell
Singularity> source ~/.rosinit
ROS_DOMAIN_ID: 10, ROS_PORT: 51010, GAZEBO_PORT: 61010
R:~$ 
```
We called `docker_shell` in CentOS and this created an Ubuntu environment.  Then we sourced the `.rosinit` file. This outputs a line showing the `ROS_DOMAIN_ID` as well as port numbers.  Each student should have unique numbers for these.  Then the new command line in your Ubuntu/ROS environment starts with a "`R:`".  This was added just to make it clear when you are in your ROS environment versus the CentOS.  Since both ROS 1 Noetic and ROS 2 Foxy are installed, to source the `setup.bash` for one use `rosunder` with either `foxy` or `noetic` as an argument like this:
```
R:~$ rosunder foxy
source /opt/ros/foxy/setup.bash
```
The default is `foxy`, so if you type it with no arguments it will source the foxy setup.bash.  The only time we will need Noetic is to play ROS 1 bags.  When you are done with ROS, you can exit this and return to your CentOS shell by typing `exit`.  

## Test your ROS Environment

Let's test that ROS is working correctly.  In VSCode open two terminal shells and start your ROS environment in each and source the `foxy` setup, just like explained above.  Then in one start the publisher demo:
```
R:~$ ros2 run demo_nodes_py talker
```
And in the other shell start the subscriber:
```
R:~$ ros2 run demo_nodes_py listener
```
Confirm that your nodes can successfully publish and subscribe.  

# Create a Link to Class Data

To allow easy access to data including rosbags, you can create a symbolic link to the data folder.  The link will act just like a folder containing the data.  To do that, first make sure you have your `av` folder in your home folder:
```
$ mkdir -p ~/av
```
Then create the link with:
```
$ ln -s /mnt/research/ece_cse_434 ~/av/data
```
Now have a look at the contents of your data folder:
```
$ ls ~/av/data
```
Additional data will be uploaded here during the semester.

# Interactive Desktop

## HPCC On-Demand
In order to display graphics, we need a separate interface, as it is not possible to display graphics in the VSCode connection.  HPCC has a convenient web-based interactive graphical interface.  This can be accessed from [https://ondemand.hpcc.msu.edu](https://ondemand.hpcc.msu.edu).  You will see this page when you connect:

![On Demand](.Images/ondemand.png)

From the Interative Apps menu, select Interactive Desktop.  You'll then be prompted to configure your Interactive Desktop like this:

![Configure Desktop](.Images/Select_Session.png)

Choose the number of hours that you will need the desktop between 1 and 4.  (Requesting more than 4 hours will put you in a separate queue which is often very slow.)  Then 4 cores will be adequate for most of what we do, and 2 GB will be plenty of memory.

After you launch the session, it will take a few minutes to start up (although could be longer as it needs to find an available node that matches your requirements). When it is ready, it will display a `Launch Interactive Desktop` button:

![Launch Desktop](.Images/Session_Ready.png)

Clicking on this button will bring up a full Desktop in your web browser.  You can start a terminal from the pull-down menu as follows:

![Terminal](.Images/Terminal.png)

## ROS in an Interactive Desktop

Running ROS in the interactive desktop it identical to what you did above in the VSCode terminal, see [Start an Ubuntu Shell](#start-an-ubuntu-shell).  Also notice that terminal windows have tabs, and this is convenient for running multiple ROS shells.

To test the graphical interface, try out the Gazebo simulator in [ROS/Gazebo Simulator](../ROS/Gazebo_Simulator.md).
___
### [Index](../Readme.md)

