# File Editing on HPCC with Visual Studio Code

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)
### [Index](../Readme.md)
## Content
* [Configure VSCode](#configure-vscode)
  * [1. Install Remote SSH Extension](#1-install-remote-ssh-extension)
  * [2. Open Connection](#2-open-a-connection)
  * [3. Configuration File](#3-configuration-file)
  * [4. Connect to Host](#4-connect-to-host)
  * [5. Remote Editing with a Workspace ](#5-remote-editing-with-workspace)
  * [6. Terminal use With VSCode](#6-terminal-with-vscode)
  * [7. Usage Time Limit](#7-usage-time-limit)

___
# Configure VSCode
We will be using a local install of Visual Studio Code to edit files remotely on the High Performance Computing Cluster (HPCC) via SSH.  Details of how to set this up are on this link: [https://docs.icer.msu.edu/Connect_over_SSH_with_VS_Code/](https://docs.icer.msu.edu/Connect_over_SSH_with_VS_Code/), although this is a bit old.  Some missing or more recent details are below

## 1. Install Remote SSH Extension: 
Run VSCode on your local machine and install the Remote SSH extension:

![Remote SSH](.Images/Remote_SSH.png)

## 2. Open a connection

![Connection](.Images/Connect.png)

## 3. Configuration File
The **first time** you connect to HPCC you will need to create a configuration file:

![Configuration file](.Images/configure_hosts_2.png)

Insert the following text, but replace `<net_id>` with your actual network ID (i.e. MSU username).
```
Host gateway
  HostName gateway.hpcc.msu.edu
  User <net_id>

Host intel14
  HostName dev-intel14
  User <net_id>
  ProxyJump gateway

Host intel18
  HostName dev-intel18
  User <net_id>
  ProxyJump gateway

Host k80
  HostName dev-intel16-k80
  User <net_id>
  ProxyJump gateway
```
Don't forget to replace all instances of `<net_id>` with your MSU user ID.  It is easy to do this with `Ctrl-H`.

Save this file and repeat step 2.

## 4. Connect to Host
Select: Connect to Host ...

![Connect to Host](.Images/connect_to_host.png)

Then choose one of: `intel14`, `intel18` or `k80`:

![Host](.Images/intel18.png)

Answer the questions including selecting `linux` as the target host, and providing your **MSU** password twice (not Engineering password).

When you are connected, you should see `SSH:intel18` or `SSH:k80` in the bottom left corner:

![Intel 18](.Images/ssh-intel18.png)

## 5. Remote Editing with Workspace

When editing files remotely in VSCode, it is easiest to use a workspace.  This will enable you to see the file system from inside VSCode, will reduce the number of times you have to re-enter your password, and enable quicker returning to your work if you close VScode.

Start by opening a folder.  I recommend your `ros_ws` folder.  Then select `File / Save Workspace As...`.  You can save the workspace to wherever you like.  You can also add additional folders to the workspace.

## 6. Terminal With VSCode

Simply select `New Terminal` under the `Terminal` menu.  This will start a CentOS terminal in a panel below the editor.  You can open any number of terminals.  Note, though, that there is no graphical user interface with these terminals.  For that see the [HPCC ROS](HPCC_ROS.md).

## 7. Usage Time Limit

The above HPCC connection uses a development node.  These are intended for code development, and not for large, computationally expensive jobs.  You will have 2 hours of CPU time available and any task exceeding that will be automatically killed.  So if you are connected for a very long time, your terminal or node or even your connection might be killed.  For more computationally expensive tasks, use the Interactive Cluster environment, described in [HPCC ROS](HPCC_ROS.md).

___
### [Index](../Readme.md)
