# ROS 2 Installation

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)
### [Index](../Readme.md)

* [Introduction](#Introduction)
* [EGR Remote Desktop](#egr-remote-desktop)
* [Install ROS 2 on your own System](#install-ros-2-on-your-own-system)
  * [Install ROS in Windows 10](#install-ros-in-windows-10)
  * [Install ROS in Windows 11](#install-ros-in-windows-11)
  * [Install ROS Ubuntu](#install-ros-in-ubuntu)
___
# Introduction

ROS 1 was initially developed for Ubuntu, and with ROS 2, official support has been extended to MacOS and Windows.  Despite this official support, ROS remains Ubuntu-centric, with somewhat less functionality in other OSes as well as additional challenges in installing code.  Nevertheless, ROS 2 on Windows functionality is sufficient for many uses and will be the primary version used in this course.  From a developer's perspective, ROS code can be easily be written that is platform independent, and thus what you learn about ROS in this course will be mostly OS agnostic.  

This class will use latest ROS 2 Long-Term Support (LTS) release, **Humble Hawksbill** released earlier in 2022, as well as **Foxy Fitzroy** released in 2020.  We will run these under the Windows 10 and will provide tailored install in engineering remote desktop.  In addition, the Turtlebots which have Raspberry Pi computers, will be running ROS 2 under Ubuntu.  

These class-provided ROS 2 versions will be pre-installed for you, and the instructor and TA will provide help with these.  You are also free to install and use ROS 2 on your own computer as it is free, open-source software.  Doing so is quite feasible if you are willing to look up online documentation.  However, you will need to do this installation on your own as supporting this outside the scope of the class.  Nevertheless, I provide some notes below to get you started on installing ROS on systems beyond what we provide for you in the course.

___
# EGR Remote Desktop

DECS has created a Windows Remote Desktop environment with ROS 2 installed.  You can access this from a web browser with this link: [remote.egr.msu.edu](https://remote.egr.msu.edu). Once you sign in you should see a window like this:

![RemoteDesktop](.Images/RemoteDesktop.png)

Select the `ROS2` virtual desktop.  Note: If you are on the campus network, you can download the RDP file and use it to access this same environment with Windows Remote Desktop, which is nicer than working in a web browser interface.  Click on the gear wheel to download the RDP file like this:

![RDP](.Images/rdp.png)

Then to connect with Windows Desktop, simply click on the RDP file and log in.

If you want to use the web interface, return to ROS2 in https://remote.egr.msu.edu and in the *Settings* tab, shown above, change your selection back to "Open resources in the browser"   

The Remote Desktop has ROS Humble Hawksbill (typically called *Humble*) pre-installed here: `C:\dev\ros2_humble`.  

# Install ROS 2 on your own System

The official instructions for installing Humble Hawksbill are here: https://docs.ros.org/en/humble/Installation.html, and include both source and binary installations on a variety of platforms.  It is preferable to install the **pre-compiled binary packages**, as compiling from source can take a few hours.  Here are some recommendations in the case you want to install ROS 2 on your system.  If you have difficulties be prepared to resolve them on your own or using Google search -- please don't ask as this is not part of the course.  The following are recommendations for a few systems:

## Install ROS in Windows 10

The Windows 10 binary installation of Humble is a little involved but quite doable.  Instructions are here: https://docs.ros.org/en/humble/Installation/Windows-Install-Binary.html.  And note: be careful to follow the instructions *exactly*, selecting all the specified options for each step as described.  Doing one step incorrectly can lead to a non-working installation, although you can go back and correct any mistakes.

You can do a simpler install of Foxy here: https://ms-iot.github.io/ROSOnWindows/GettingStarted/SetupRos2.html.  

[Optional] Install Windows Terminal from the Windows Store as this gives you a tabbed environment for your PowerShells which is very useful for managing multiple open shells needed by ROS.

## Install ROS in Windows 11

Unfortunately the official Humble Hawksbill installation does not support Windows 11.  The following are two options available to Windows 11 users (that also work with Windows 10):

You can install an older version, Foxy, in just a few steps from here: https://ms-iot.github.io/ROSOnWindows/GettingStarted/SetupRos2.html.  The nice thing about this install is that it is very simple and quick.  The downside is that it does not advertise as supporting Windows 11 and from my experience a few of the commands don't work, including the Gazebo simulator which we'll use in this course.

To get Humble Hawksbill, probably the easiest way is to install the Ubuntu version within WSL.  Now that WSL within Windows 11 supports GPU operations as well as the graphical user interface, a ROS install in WSL is a good experience.  These are the steps needed:
* Install WSL 2: https://docs.microsoft.com/en-us/windows/wsl/install
* Install Ubuntu 22.04 from the Windows Store.
* Follow instructions below for ROS 2 installation on Ubuntu 22.04

[Optional] Install Windows Terminal from the Windows Store as this gives you a tabbed environment for your PowerShells or Ubuntu Bash shells, which is very useful for managing multiple open shells needed by ROS.


## Install ROS in Ubuntu

ROS was initially designed for Ubuntu, and so it is not surprising that installation of Ubuntu binaries is  straightforward. Since 2020 there has been one release per year of ROS 2, each targetting a LTS (Long Term Support) version of Ubuntu.  These ROS 2 versions are summarized here:

| ROS 2 Distro | Release Date | EOL Date | Ubuntu Version |
|---|---|---|---|
Humble Hawksbill | May 2022 | May 2027 | 22.04 (Jammy Jellyfish) |
| Galactic Geochelone | May 2021 | Nov 2022 | 20.04 (Focal Fossa) |
| Foxy Fitzroy | June 2020 | May 2023 | 20.04 (Focal Fossa) |

Instructions for installing the most recent version, Humble Hawksbill are here: https://docs.ros.org/en/humble/Installation/Ubuntu-Install-Debians.html


___
### [Back to Index](../Readme.md)
