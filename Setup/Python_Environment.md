# Python Environment

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)
### [Index](../Readme.md)
## Contents
* [Python Preliminaries](#python-preliminaries)
* [Python Version for Humble](#python-version-for-humble)
* [A ROS Python Environment](#a-python-environment)
* [Create a Virtual Environment](#create-a-virtual-environment)
  * [Activate Your Virtual Environment](#activate-your-virtual-environment)
  * [Add Packages to Your Virtual Environment](#add-packages-to-your-virtual-environment)
  * [Quitting Your Virtual Environment](#quitting-your-virtual-environment)
___


# Python Preliminaries

This course will assume that you are proficient in Python.  For a quick, but intense review, see the [Python Intro Notes](https://github.com/dmorris0/python_intro).  

# Python Version for Humble

ROS Humble, as well as earlier versions Foxy and Galactic, use Python 3.8.3.  More recent Python version may work fine, but to be safe it's a good idea to use the designated version.  This version of Python has been pre-installed on the EGR remote desktop in `C:\Python38` which is added to the PATH so it can be run directly from PowerShell.

# A ROS Python Environment

A number of Python libraries are pre-installed in the base ROS Python environment.  These include OpenCV and Matplotlib.  Try out Python and test that these libraries are available as follows:
````
$ python
Python 3.8.3 (tags/v3.8.3:6f8c832, May 13 2020, 22:37:02) [MSC v.1924 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> import cv2 as cv
>>> import matplotlib.pyplot as plt
>>> exit()
````
If you get no errors you are good to go.

# Create a Virtual Environment

Now it is straightforward to add additional packages to your Python environment using the command:
```
$ python -m pip install <package_name>
```
**However, do not do this.**  The reason is that there are a plethora of packages and package versions, many of which create conflicts with others.  Instead, when you wish to install a new package or group of packages such as for this class, create a virtual Python environment and install them within this environment.  You can create any number of virtual environments and easily switch between them, as described in the following.

I recommend creating a folder to store your virtual environments as otherwise it is easy to lose track of them.  Any folder name and any location will work, and for this class I recommend using a folder on your M drive, and in particular: `M:\av\venvs`.  To create this within PowerShell you can type:
```
md M:\av\venvs
```
When you are ready to create a virtual environment, first `cd` to this location:  
```
$ cd M:\av\venvs
```
Next choose a name.  For this class let's all use the name `av`.  Then to create your `av` virtual environment type:
```
python -m venv --system-site-packages av
```
If you type 'dir', you'll see that there is now a subfolder called `av` in your `venvs` folder that contains your new environment.  Here we included the `--system-site-packages` option.  This means your new virtual environment will inherit packages from the top level Python environment, which we want to do in this case as the top level environment contains a number of packages that we will use.  That way we do not need to reinstall them within the `av` environment.

## Activate Your Virtual Environment
Now when you want to use the `av` environment, you can activate it with:
```
M:\av\venvs\av\Scripts\activate.ps1
```

As you can see this is a little tedious to type each time you want to activate this environment. Thus I created a function called `act` (short for activate) what will ease your typing.  This is defined in the PowerShell startup script explained in [Shell_Config](Shell_Config.md).  This sets the `$venvfolder` variable equal to the full path to your `venvs` folder.  To use it type `act` like this:
```powershell
PS M:\> act


    Directory: C:\Users\dmorris\source\venvs


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----         7/23/2022  11:43 AM                av

```
Here I have shown the full PowerShell prompt where I typed `act`.  This shows all the folders in my `venvs` folder, which will be all the virtual environments I have defined.  To actually activate my environment you can type `act <virtual_environment_name>` like this:
```powershell
PS M:\> act av
(av) PS M:\>
```
You'll notice that once the `av` virtual environment has been activated, all powershell prompts begin with `(av)`. That way you know if you start Python, it will be using that virtual environment, or if you install a package, it will install within that virtual environment.  


## Add Packages to Your Virtual Environment

To add packages to your `av` environment, **first** activate it so that you see `(av)` at the start of your terminal line.  You can then install packages with `pip` and these will be added to this environment.  For example:
```bash
(av) PS M:\> python -m pip install scikit-learn
```
From now on `scikit-learn` will be available when you activate `av`.  Let's test it:
```powershell
(av) PS M:\> python
Python 3.8.3 (tags/v3.8.3:6f8c832, May 13 2020, 22:37:02) [MSC v.1924 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> import scipy
>>> exit()
```

## Quitting Your Virtual Environment

To quit your virtual environment, simply type:
```
deactivate
```

___
Next, install VSCode and try out the Python debugging examples in [VSCode.md](#VSCode.md).
