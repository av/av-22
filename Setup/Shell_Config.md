# Configure the Command Line Shell

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)
### [Index](../Readme.md)
___
## Contents
* [Introduction](#introduction)
* [Windows PowerShell](#windows-powershell)
  * [PowerShell Startup Script](#powershell-startup-script)
  * [Trouble Shooting Startup](#trouble-shooting-startup)
* [Ubuntu](#ubuntu)
  * [Ubuntu Commands](#ubuntu-commands)
  * [Ubuntu Startup Script](#ubuntu-startup-script)
* [Test ROS](#test-ros)
___

## Introduction
ROS relies heavily on commands typed into a terminal, and has relatively limited graphical user interface (GUI) options.  Hence it is important to become familiar with terminal commands, including Windows and/or Linux commands  as well as ROS commands.  This document steps you through how to configure your environment for running ROS in either Windows or Linux.  It assumes you already have an installed version of ROS 2 as described in the [Installation](Installation.md) document.

## Windows PowerShell

It is possible to use either the *Command* shell or *PowerShell*.  The latter is more powerful and we will use this most of the time.  The PowerShell can be run as a stand-alone application or within Windows Terminal or within VSCode -- all are equivalent, although both Windows Terminal and VSCode provide tabs to organized multiple windows, which is very useful as you'll likely have many windows open simultaneously.

First find which ROS distro you have installed and the location where it is installed.  Often it is a subfolder within `C:\opt`, but can be located elsewhere.  The EGR remote desktop version has it installed here:
```
C:\dev\ros2_humble
```
In order to run ROS commands you need to activate the underlay.  In PowerShell you will run the `local_setup.ps1` command in the ROS folder.  Simply execute `<path_to_distro>\local_setup.ps1`.  For this distro that means you will type this into PowerShell
```
C:\dev\ros2_humble\local_setup.ps1
```
If PowerShell gives a permission error running this file, you can make it directly callable with:
```
Unblock-File C:\dev\ros2_humble\local_setup.ps1
```
You will need to activate the underlay (or the overlay as we'll see later) in every terminal you run ROS.  

Let's check that sourcing your underlay has run properly and set a collection of ROS environment variables.  For this use the `Get-ChildItem`, or its alias `gci`, as follows:
```
gci env: | findstr ROS
COMPUTERNAME                   RDSVDIROS2-2
ROS_DISTRO                     humble
ROS_LOCALHOST_ONLY             0
ROS_PYTHON_VERSION             3
ROS_VERSION                    2
```
Or to get a particular environment variable:
```
Get-ChildItem -path env:ROS_VERSION

Name                           Value
----                           -----
ROS_VERSION                    2
```

### PowerShell Startup Script

Now sourcing the underlay as done above is a bit tedious, and is confusing if you use multiple computers with different install locations for ROS.  A solution to this is to define a function in a startup script that knows where your ROS install is on each system and makes it easy to source the overlay.  I created a function to source the underlay called `rosunder` with an optional argument to specify the distribution.  How I did this is described in the remainder of this section.

PowerShell will initialize commands from your profile file. To locate the profile file type:
```
echo $profile
\\filer1.egr.msu.edu\dmorris\My Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1
```
You can see where my profile file is located and yours will have its own path.  In this case the file is on a shared drive, which means that it will be used to initialize PowerShell from multiple computers that are linked to it.  That complicates somewhat the setup of the file as install paths may differ between computers.  In the example script below I check the computer name and set appropriate command shortcuts according to the computer.  

Edit your profile file (or create it if it doesn't exist) using the command from your PowerShell:
```
code $profile
```
Then copy and add the following lines to this file.  If you are using the Remote Desktop, you don't have to change anything.  If you are using your home computer, you can replace the matching condition: `elseif ($hostname -Match 'ECE170')` with your computer name and follow this with appropriate paths for your system.

```
# Use hostname to tailor commands for different systems
$hostname = get-content env:computername

# Set up ROS and Python evironment
# Adjust the following for your system(s):
if ($hostname -Match 'RDSVDIROS')                     # ROS2 Remote Desktop for ECE-CSE 434
{
  $roshumble = "C:\dev\ros2_humble\local_setup.ps1"   # Startup for ROS 2 Humble
  $rosfoxy = "C:\opt\ros\foxy\x64\local_setup.ps1"    # Startup for ROS 2 Foxy
  $rosdefault = 'humble'                              # Make Humble the default ROS 2
  $venvfolder = "M:\av\venvs"                         # Path to virtual environment folder
}
elseif ($hostname -Match 'ECE170')                    # Settings for my office computer
{
  $roshumble = "F:\opt\ros\humble\local_setup.ps1" 
  $rosdefault = 'humble'
  $venvfolder = "D:\DMorris\venvs" 
} 
else
{
  Write-Output "Warning hostname: $hostname not recognized. Update your `$profile` file."
  $venvfolder = "$home\venvs"
}

# ---
# ROS helper commands
# Source the underlay for 'foxy' or 'humble'
# $roshumble and/or $rosfoxy must be defined above
# Also, $rosdefault should be either 'foxy' or 'humble'
# Usage:
#  rosunder humble
#  rosunder foxy
#  rosunder
function rosunder
{
  param ( [string]$rosselect = $rosdefault )
  if ($rosselect -eq 'humble')
  {
    $setup = $roshumble
  } 
  elseif ($rosselect -eq 'foxy')
  {
    $setup = $rosfoxy
  }
  if ("" -eq $setup -or $null -eq $setup)
  {
    Write-Output "No repo in `$profile` for: $rosselect"
  }
  else 
  {
    Write-Output $setup
    & $setup  
  }
}

# ---
# Python helper commands
# Activate python virtual environments.  Call with:
#   act <virtualenv_name>
# Or call without arguments to get a list of virtual environments
function act([string]$virtualenv)
{
  if ($virtualenv -eq "")
  {
    Get-ChildItem $venvfolder
  } else 
  {
    $cmd = $venvfolder + "\" + $virtualenv + "\Scripts\activate.ps1"    
    Write-Output $cmd
    & $cmd
  }
}

```
Saved your updated profile file and open a *new* PowerShell so that this file is sourced.  To confirm that it is working, try calling `rosunder` like this:
```
PS M:\> rosunder
C:\dev\ros2_humble\local_setup.ps1
```
You can also view the ROS environment variables as explained above to confirm ROS is properly initialized.

### Trouble Shooting Startup

If, when you start a new PowerShell, you get an error saying the startup script `"cannot be loaded because running scripts is disabled on this system"`, then run the following command and then start a new PowerShell:
```
Set-Executionpolicy unrestricted
```

If there are errors in the above or if at the top of the new PowerShell window you might see a line like this:
```
Warning hostname: DM-O not recognized. Update your $profile file.
```
That means your hostname test is not working.  Type
```
echo $hostname
```
Then you should update your profile file to check for this hostname in one of the `elseif ($hostname -Match <your_hostname>)` lines.  After you make this change, start a new PowerShell, and you should no longer see the same output.

On the other hand, if you do not get the warning and when you type `echo $hostname` you get nothing, then your profile file is not being called.  Go back and make sure your file has the same name as you get from `echo $profile`.  

## Ubuntu

Early versions of ROS had official support only for Ubuntu.  And still Ubuntu provides the simplest environment for installing ROS.  Ubuntu runs on the Raspberry Pi computers on the Turtlebots, and ROS will run within Ubuntu on the Turtlebots.  The following is a brief summary of commands you may find helpful when you configure your Turtlebots.  

As we will see later, Turtlebots will be accessed through a `ssh` command.  This will give you `Bash` shell interface, somewhat similar to PowerShell.  Within this you can source the underlay and overlay and start ROS nodes, just like in PowerShell.

A key file is the startup script which is named `.bashrc`.  Whenever a shell is created, this script will run.  Thus you can use it to configure commands and your ROS environment.  In this class your `.bashrc` file will be pre-configured for you.  Nevertheless, you may wish to examine it and add to or modify it.  It is located in the user home folder, referred to with `$HOME` or `~`.  Thus to edit it type:
```
nano ~/.bashrc
```
More details will be given when we start working with the Turtlebots.

___
### Ubuntu Commands

In Ubuntu you'll be working primarily in a `Bash` shell.  The following are a set of commands that you may find helpful.  There's no need to memorize them, but if you are not already familiar with Linux it is worth trying them out to get a feel for the environment.


| Linux command | Description |
| ------------- | ----------- |
| `cd /path/to/directory` | Change to specified directory |
| `cd` and `cd ~` and `cd /home/$USER` | Change to your Ubuntu home folder  |
| `pwd` | Print working directory |
| `ls`  | List files in working directory |
| `ls -lhrt` | List files sorted in reverse time and show attributes |
| `ln -s <source_path> <target>` | Create a symbolic link to source and put it in target |
| `printenv` | List all the environment variables |
| `printenv` &#124; `grep ROS` | Ditto, but show only lines that include `ROS` in them.  Useful for confirming ROS underlay and overlay, and settings. |
| `echo $<ENV_NAME>` | Displays value of a bash environment variable | 
| `export <ENV_NAME>=<value>` | Set a bash environment variable.  NOTE: make sure there are **no** spaces around the `"="` | 
| `htop` | Shows currently running processes with memory and CPU usage (use `q` to exit) |
| `ctrl-c` | Keyboard interrupt.  Most ROS apps exit gracefully with this interrupt |
| `gedit <filename>` | Edit text files.  Note it is usually better to use VSCode for Python |
| `sudo apt update` | This refreshes the app repository lists.  Always do this before `sudo apt updgrade` and `sudo apt install`.  |
| `sudo apt upgrade` | If there are new versions of currently installed packages, this will upgrade them. |
| `sudo apt install <app_names>` | Install one or more apps if not already installed |
| `source <script>` | This executes the commands in a file `<script>` within the current shell environment |
| `. <script>` | Ditto.  Notice the space after the "`.`" |
| `alias <new_name>="<command>"` | Define an alias for a command (see below) |
| `alias` | List all currently defined aliases |
| `grep -rnHI "text to find"` | Recursively searches through text files in a folder for `"text to find"` |

### Ubuntu Startup Script

If you are using your own Ubuntu environment to run ROS in, either stand-alone or within WSL (and not including on the Turtlebot), you may wish to add some alias and function definitions to the `.bashrc` file.  Adding the following lines to the bottom of this file will give similar functionality to those added to the PowerShell startup script:
```
alias rosunder="source /opt/ros/humble/setup.bash"          # Source the underlay
alias sbash='source $HOME/.bashrc'                          # Source the ~/.bashrc file

# Command to activate selected virtual environment:
VENV_FOLDER=$HOME/venvs                                     # Change this as appropriate
act() {
  if [ $# -eq 0 ] 
  then
    ls $VENV_FOLDER                       # If no arguments, display all virtual environments
  else
    source $VENV_FOLDER/$1/bin/activate   # Activate selected virtual environment
  fi
}
```

# Test ROS

Let's test that ROS is configured correctly.  We will run a ROS publisher node that publishes messages on topic and a subscriber that reads these messages.  

Open a shell window and source the underlay using `rosunder`.  Then type:
```
ros2 run demo_nodes_py talker
```
Open another shell and again source the underlay using `rosunder`.  Then type:
```
ros2 run demo_nodes_py listener
```
You should see messages being passed from the talker to the listener.  

___
### [Back to Index](../Readme.md)
