# Debug with VSCode

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)
### [Index](../Readme.md)
___
## Contents
* [IDE Installation](#ide-installation)
  * [Run](#run)
* [Debug Preliminaries](#debug-preliminaries)
  * [Folders](#folders)
  * [Workspaces](#workspaces)
  * [Working Directory](#working-directory)
* [Example: Python Debugging](#example-1-python-debugging)


___
# IDE Installation

I recommend using Visual Studio Code for this class.  It is pre-installed on the EGR Remote Desktop, and is easy to install in your own system from: [https://code.visualstudio.com/download](https://code.visualstudio.com/download).


___
# Debug Preliminaries

VSCode provides powerful debugging capabilities.  You can step through code and examine variable contents.  It is easy to debug ROS nodes.  This document steps through the process of debugging a ROS node.  It also demonstrates how to pass arguments to your code when you run a debug session. But first, a couple preliminaries before you start debugging.  

## Preliminaries 
First set up a Virtual Python environment, as explained in [Setup/Python_Environment.md](Python_Environment.md)

## Folders
In VSCode, rather than opening a single file, it is more usual to open a folder (from the File Menu).  This is typically the source folder for a project.  You can set debug commands for various files within this folder.

## Workspaces
You can also create a workspace, that contains one or more folders, from the File menu.  A workspace can share properties and folders with files can be viewed in the Explorer on th left.  Workspaces particularly useful for remote-SSH sessions.  Adding a folder will require inputting your password (unless you have set up a public/private key).  But once you have added the folder to your workspace, you can open and add files to it without re-entering your password. 

## Working Directory

When running debug, your Python code runs relative to the Folder or Workspace definition.  If you are opening a file in your code, it can be tricky to know which folder one needs to specify it with repect to.  A simple way to find the working folder is as follows:

1. Run your code and put a break at the line you are concerned with.
2. Click on 'DEBUG CONSOLE' in the top ribbon of the OUTPUT window.  
3. Type:
```bash
>>> import os
>>> os.getcwd()
```

___
# Example 1: Python Debugging

This is a quick-start example of debugging Python in VSCode.  See [Example 2](#Example-2-ROS-Python-Debugging) below for a second, more in-depth example that includes a ROS node.

Following the directions in the [VSCode install docs](VSCode_Install.md), open VSCode in your home OS, and begin a remote session in your Ubuntu environment.  This may be via `ssh`, `WSL` or `Docker`.  

*  Open file [python/hello_user.py](python/hello_user.py) in your VSCode editor.  

If you are viewing this document on the Gitlab website or on Windows, you'll need to transfer this file to your Ubuntu instance.  The easiest way to do this is by cloning this repo in Ubuntu.  Alternatively, you could copy it via a mounted drive.  In WSL and Docker you can likely find a mount of your home OS in `/mnt/`. (In some cases copying source code from Windows can be problematic as Windows uses a carriage-return and line-feed at the end of each line, unlike linux see [Trouble_shooting](Trouble_Shooting.md).) 

You should now have a Python file in your VSCode window.  The first few lines of it are shown below.  It is a simple Python class that uses OpenCV to display a rotating message in a window.  Make sure you understand how it works, as you will be implementing similar programming problems.
```python
#!/usr/bin/env python
'''
    hello_user.py 

    This greets the user with a rotating message
'''
import numpy as np
import cv2
import os

class ImageRotator:

    def __init__(self, message=''):
        self.img = []
        self.angle = 0.
        self.create_hello_image(message)

# <Open python/hello_user.py to see rest of code>
```
When you open a Python file you may be presented with a warning about selecting a Python interpreter like this:

![Select Interpreter](.Images/pythonSelectInterpreter.png)

Click on the Yellow highlighted text to get an option like this from which you can select which Python interpreter you want to execute your code:

![Select Interpreter](.Images/pythonSelectInterpreter2.png)

After you select your interpreter, possibly the `work` or `av` environment you created above, you should see your interpreter at the bottom of the VSCode window:

![Select Interpreter](.Images/pythonSelectInterpreter3.png)

By clicking on this you can change your interpreter any time you want.


Now, start Debug from the left bar of VSCode:

![Run Debug](.Images/debug.png) 

If you want to pass arguments to your code, you would select `create a launch.json file`, and set up a configuration for your code (see Example 2 below).  In this case you can simply click on `Run and Debug` above, and then select: `Python File`:

![Run Python File](.Images/pythonFile.png)

If you get an error in the `import` lines, then as in Section 1, ensure that the underlay has been sourced in the VSCode terminal using: `source /opt/ros/noetic/setup.bash`.   Sourcing the underlay is sufficient to get the libraries.  If you are debugging a ROS node (as opposed to simply a Python file like this one), then you will want to source the overlay, namely: `source ~/catkin_ws/devel/setup.bash`.   When your code runs you should see an output like this:

![Hello](.Images/hello.png)

To quit, click on the window and press `q`.

___
### [Back to Index](../Readme.md)
