# Setup Guide for VirtualBox

### Author: [Daniel Morris](https://www.egr.msu.edu/~dmorris)
### [Index](../Readme.md)
* [Install Method 1: Pre-made Image](#install-method-1-pre-made-image)
* [Install Method 2: Create Your Own Image](#install-method-2-create-your-own-image)
* [Set `ROS_DOMAIN_ID`](#set-ros_domain_id)
___

VirtualBox is a free and relatively simple way to run one OS within another in a Virtual Machine (VM).  Being free, it has some limitations especially no ability to leverage the GPU from the host computer.  It also runs the virtual OS relatively slowly compared to a direct intall, and uses a fair amount of resources.  However, it does give a full Ubuntu instance for the course and so is a relatively simple way to set up a complete environment for the course.  A key advantange over WSL is that it can connect to the same network as your host computer, and so can be used to communicate with the TurtleBots via WiFi (unlike WSL).

There are two ways you can get an Ubuntu + ROS instance with VirtualBox. The first is the most straightforward and involves downloading a pre-create image with everything installed.  For some this image did not work, and so the second option is to create your own Ubuntu image, install ROS and additional libraries.  

# Install Method 1: Pre-made Image

The following instructions install a pre-made Ubuntu + ROS image.  

1. Download and install VirtualBox from: [https://www.virtualbox.org/](https://www.virtualbox.org/)
2. Download the the VM image called [Ubuntu 20.ova](https://drive.google.com/file/d/1GeB6LEpR3tz0VwcrMhUlcKAkzfkX7J3Q/view?usp=sharing).  It is over 6 GB.  It is also available on the EGR Network at these locations:
    * \\\\cifs\courses\CSE\434\Shared\VirtualBox\Ubuntu_20.ova
    * \\\\cifs\courses\ECE\434\Shared\VirtualBox\Ubuntu_20.ova
3. Run VirtualBox and import the above `.ova` file.  This will create VM called `Ubuntu 20`.   ![New VM](.Images/VirtualBox.jpg) 
4. Before running it, set the following preferences:
    - Under `Settings`, `Network`, set your Wi-Fi adapter to Bridge mode if not already set this way. Disable NAT if is selected.
    - Also, under the same `Settings`, `Network`, select `Advanced` and click on the `refresh` icon for the MAC address.  This is important so that we don't have multiple VirtualBoxes with the same MAC address.  ![MAC](.Images/BridgedNetwork.jpg)
    - If your computer as muliple cores, set the CPU count to at least 2
    - You may wish to enable copying between the VM and the host under 'Settings', 'General', 'Advanced'
    - Set the RAM to at least 2048 MB, and preferably 4096 MB if your laptop has at least 6000 MB RAM available.
    - You can start your new Ubuntu instance.
5. Debugging Virtual Box 
    * If your Ubuntu instance will not run, make sure that
        - Virtualization is enabled in your bios
        - You may need to disable [Hyper-V](https://docs.microsoft.com/en-us/troubleshoot/windows-client/application-management/virtualization-apps-not-work-with-hyper-v)
    * Error on Mac: ‘The Installation Failed’,  [try this](https://medium.com/@DMeechan/fixing-the-installation-failed-virtualbox-error-on-mac-high-sierra-7c421362b5b5)


# Install Method 2: Create Your Own Image

1. Download and install VirtualBox from: [https://www.virtualbox.org/](https://www.virtualbox.org/)
2. Download Ubuntu 20.04 (Focal Fossa) as a `.iso` file: https://releases.ubuntu.com/focal/
3. Start VirtualBox and click on `New` and follow the instructions selecting default options for most things.  A few things you'll want to set:
    * Set the RAM to at least 2048 MB, and preferably 4096 MB if your laptop has at least 6000 MB RAM available.
    * For disk drive, choose approximately 24 GB.
    * For Processors choose 2 or more, as long as you have extra to spare.
4. When you have your new VM, then: 
    * Under `Settings`, `Network`, set your Wi-Fi adapter to Bridge mode if not already set this way. Disable NAT if is selected.
    * Under `Storage` add an optical drive and select the Ubuntu `.iso` file you just downloaded.  
![Optical Drive](.Images/iso-drive.jpg)
5. Click on `Start`, and then follow the instructions for installing Ubuntu. 
6. If the window size is fixed and small, you can have it variable as follows.
    * Click on `Input` / `Insert Guest Additions CD` and allow the script to run
![Guest Additions](.Images/GuestAdditions.jpg)
    * Then under `View` menu you should be able to enable `Auto-Resize`.
7. Install ROS 2 Foxy: https://docs.ros.org/en/foxy/Installation/Ubuntu-Install-Debians.html
    * You'll want to select the ros-foxy-desktop version
8. Install a number of additional ROS-related libraries
```
sudo apt update
sudo apt install -y python3-pip python3-tk git python3.8-venv wget curl libopencv-dev python3-opencv

# ROS Foxy tools including Gazebo, Cartographer and Navigation2
sudo apt install -y ros-foxy-gazebo-* ros-foxy-cartographer \
    ros-foxy-cartographer-ros ros-foxy-navigation2 ros-foxy-nav2-bringup

# Turtlebot3 packages
sudo apt install -y ros-foxy-dynamixel-sdk \
    ros-foxy-turtlebot3-msgs ros-foxy-turtlebot3 ros-foxy-turtlebot3-simulations
```
9. Update your `.bashrc` file by adding the following lines at the end:
```
# Deconflict ROS and Gazebo instances by adjusting the below:
export ROS_DOMAIN_ID=0             # replace 0 with your assigned number.  **No spaces around the "="
export ROS_PORT=51000              # replace 51000 with the sum of 51000 and your ROS_DOMAIN_ID
export GAZEBO_PORT=61000           # replace 61000 with the sum of 61000 and your ROS_DOMAIN_ID
export GAZEBO_MASTER_URI=http://localhost:$GAZEBO_PORT
export ROS_MASTER_URI=http://localhost:$ROS_PORT
echo "ROS_DOMAIN_ID: $ROS_DOMAIN_ID, ROS_PORT: $ROS_PORT, GAZEBO_PORT: $GAZEBO_PORT"

# Avoid compile warnings with colcon build:
export PYTHONWARNINGS=ignore:::setuptools.command.install,ignore:::setuptools.command.easy_install,ignore:::pkg_resources

echo "source /opt/ros/foxy/setup.bash"
source /opt/ros/foxy/setup.bash
PS1="R:\w\$ " # Setting PS1 to show full path

export PROMPT_DIRTRIM=4 # Trimming path to 4 directories
```


# Set `ROS_DOMAIN_ID`

As usual, adjust the `ROS_DOMAIN_ID`, `ROS_PORT` and `GAZEBO_PORT` in your `.bashrc` file according to the number given to you.  If you want to control the Turtlebot with VirtualBox, you should set `ROS_DOMAIN_ID` to be the same number as in your Turtlebot.

Be aware that if your teammates are also setting their `ROS_DOMAIN_ID` to the same number, that there is potential for multiple people or nodes to be attempting to control the Turtlebot at the same time.  That is okay, but just be aware of that, as some nodes may conflict with each other.

___
### [Back to Index](../Readme.md)
